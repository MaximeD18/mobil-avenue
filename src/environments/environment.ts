// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebaseConfig : {
    apiKey: "AIzaSyBB24m2fpePIajJ6jbNZ8YHd8Y1MwFlXVk",
    authDomain: "mobil-avenue-revendeur.firebaseapp.com",
    databaseURL: "https://mobil-avenue-revendeur.firebaseio.com",
    projectId: "mobil-avenue-revendeur",
    storageBucket: "mobil-avenue-revendeur.appspot.com",
    messagingSenderId: "251995888699",
    appId: "1:251995888699:web:099bcfee9d2ed4c6a4d01e",
    measurementId: "G-PFN1Q438WZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
