import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Components
import { WelcomeComponent } from './Components/welcome/welcome.component';
import { TableComponent } from './Components/table/table.component';

import { AuthentificationService } from './services/authentification.service'
import { FormsModule } from '@angular/forms';
// Firebase
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FormComponent } from './Components/form/form.component';
import { RedirectTableOrFormComponent } from './Components/redirect-table-or-form/redirect-table-or-form.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    TableComponent,
    FormComponent,
    RedirectTableOrFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule, // Only required for database features
    AngularFireAuthModule, // Only required for auth features,
    AngularFireStorageModule // Only required for storage features
  ],
  providers: [AuthentificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
