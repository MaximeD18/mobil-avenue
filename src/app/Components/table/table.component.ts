import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../../services/authentification.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  showHideInputs:boolean;
  constructor(public authenticationService : AuthentificationService) { }

  ngOnInit(): void {
  }

}
