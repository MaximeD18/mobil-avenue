import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectTableOrFormComponent } from './redirect-table-or-form.component';

describe('RedirectTableOrFormComponent', () => {
  let component: RedirectTableOrFormComponent;
  let fixture: ComponentFixture<RedirectTableOrFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedirectTableOrFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectTableOrFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
