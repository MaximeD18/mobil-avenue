import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';


// Datas 
import { Users } from './users.service';
import { Days } from './days.service';


@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {
  days = Days;
  userDataObservable: Observable<firebase.User>;
  userData : any;
  constructor(
    private angularFireAuth: AngularFireAuth, 
    private angularFirestore : AngularFirestore, 
    private router : Router
    ) 
  { 
    /* Saving user data in localstorage when 
    logged in and setting up null when logged out */
    this.angularFireAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('users', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('users'));
      } else {
        localStorage.setItem('users', null);
        JSON.parse(localStorage.getItem('users'));
      }
    })
    this.userDataObservable = angularFireAuth.authState;
  }
   
/* Sign up */
SignUp(email: string, password: string) {
  return auth().createUserWithEmailAndPassword(email, password)
  .then((res) => {
    this.SetUserData(res.user);
    this.router.navigate(['table']);
    console.log('You are Successfully signed up!', res.user);
  })
  .catch(error => {
    console.log('Something is wrong:', error.message);
  });
  }
   
  /* Sign in */
  SignIn(email: string, password: string) {
  return auth().signInWithEmailAndPassword(email, password)
  .then(res => {
    this.SetUserData(res.user);
    this.router.navigate(['table']);
    console.log('You are Successfully logged in!', res);
  })
  .catch(err => {
    console.log('Something is wrong:',err.message);
  });
  }
   
  /* Sign out */
  SignOut() {
  auth().signOut();
  this.router.navigate(['welcome']);
  }

  SetUserData(user: firebase.User) {
    const userRef: AngularFirestoreDocument<any> = this.angularFirestore.doc(`users/${user.uid}`);
    const userData: Users = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
    }
    console.log("SetUserData a finit de se déclencher")
    return userRef.set(userData,
        {merge : true}
    )
    .catch(err => {
      console.log('Something is wrong:',err.message);
    })
  }
}
